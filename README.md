# VPC PEERING module

Creates VPC peering connections between two VPC in different accounts.  
Then add routes to private subnets in private table in each account.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| auto_accept_peering | Accept the peering (both VPCs need to be in the same AWS account) | `bool` | false | no |
| peer_region  | The region of the accepter VPC of the VPC Peering Connection. Auto_accept_peering must be false | `string` | n/a | yes |
| requester_vpc_id | The ID of the requester VPC | `string` | n/a | yes |
| accepter_vpc_id | The ID of the VPC with which you are creating the VPC Peering Connection. | `string` | n/a | yes |
| role_arn | Assumed role for accessing accepter account | `string` | n/a | yes |
| accepter_private_subnets_cidr_blocks | List of IPv6 cidr_blocks of private subnets in an IPv4 enabled VPC of accepter ACC | `list(string)` | n/a | yes |
| accepter_private_route_table_ids | List of IPv6 cidr_blocks of private subnets in an IPv4 enabled VPC of accepter ACC | `list(string)` | n/a | yes |
| requester_private_route_table_ids | List of IPv6 cidr_blocks of private subnets in an IPv4 enabled VPC of requester ACC | `list(string)` | n/a | yes |
| requester_private_subnets_cidr_blocks | List of IPv6 cidr_blocks of private subnets in an IPv4 enabled VPC of requester ACC | `list(string)` | n/a | yes |
| requester_dns_resolution | Allow a local VPC to resolve public DNS hostnames to private IP addresses when queried from instances in the peer VPC. | `bool` | false | no |
| requester_link_to_peer_classic | Allow a local linked EC2-Classic instance to communicate with instances in a peer VPC. This enables an outbound communication from the local ClassicLink connection to the remote VPC. This option is not supported for inter-region VPC peering. | `bool` | false | no |
| requester_link_to_local_classic | Allow a local VPC to communicate with a linked EC2-Classic instance in a peer VPC. This enables an outbound communication from the local VPC to the remote ClassicLink connection. This option is not supported for inter-region VPC peering. | `bool` | false | no |
| accepter_dns_resolution | Allow a local VPC to resolve public DNS hostnames to private IP addresses when queried from instances in the peer VPC. | `bool` | false | no |
| accepter_link_to_peer_classic | Allow a local linked EC2-Classic instance to communicate with instances in a peer VPC. This enables an outbound communication from the local ClassicLink connection to the remote VPC. This option is not supported for inter-region VPC peering. | `bool` | false | no |
| accepter_link_to_local_classic | Allow a local VPC to communicate with a linked EC2-Classic instance in a peer VPC. This enables an outbound communication from the local VPC to the remote ClassicLink connection. This option is not supported for inter-region VPC peering. | `bool` | false | no |

## Outputs

| Name | Description |
|------|-------------|
| vpc_peering_connection_id | Id of peering connections | 
