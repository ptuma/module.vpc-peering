locals {
  role_arn                              = "arn:aws:iam::123456789123:role/lukapo_administrator"
  requester_vpc_id                      = module.vpc1.vpc_id
  accepter_vpc_id                       = module.vpc2.vpc_id
  accepter_private_subnets_cidr_blocks  = module.vpc1.private_subnets
  accepter_private_route_table_ids      = module.vpc1.private_route_table_ids
  requester_private_route_table_ids     = module.vpc2.private_route_table_ids
  requester_private_subnets_cidr_blocks = module.vpc2.private_subnets
  vpc_cidr_1                            = "10.20.0.0/16"
  vpc_cidr_prefix_1                     = "10.20"
  project                               = "test"
  environment                           = "example"
  aws_region                            = "eu-central-1"
  vpc_cidr_2                            = "10.30.0.0/16"
  vpc_cidr_prefix_2                     = "10.30"
}

provider "aws" {
  region = "eu-central-1"
}

module "vpc1" {
  source          = "git::https://gitlab.lukapo.com/terraform/aws/module.vpc?ref=2.0.15"
  aws_region      = local.aws_region
  vpc_name        = "vpc-${local.project}"
  cidr            = local.vpc_cidr_1
  azs             = ["${local.aws_region}a"]
  private_subnets = ["${local.vpc_cidr_prefix_1}.5.0/24"]
  public_subnets  = ["${local.vpc_cidr_prefix_1}.0.0/24"]
  tags = {
    Project     = local.project
    Environment = local.environment
  }
}

module "vpc2" {
  source          = "git::https://gitlab.lukapo.com/terraform/aws/module.vpc?ref=2.0.15"
  aws_region      = local.aws_region
  vpc_name        = "vpc-${local.project}"
  cidr            = local.vpc_cidr_2
  azs             = ["${local.aws_region}a"]
  private_subnets = ["${local.vpc_cidr_prefix_2}.5.0/24"]
  public_subnets  = ["${local.vpc_cidr_prefix_2}.0.0/24"]
  tags = {
    Project     = local.project
    Environment = local.environment
  }
}


module "vpc-peering" {
  source                                = "../"
  role_arn                              = local.role_arn
  peer_region                           = local.aws_region
  requester_vpc_id                      = local.requester_vpc_id
  accepter_vpc_id                       = local.accepter_vpc_id
  accepter_private_subnets_cidr_blocks  = local.accepter_private_subnets_cidr_blocks
  accepter_private_route_table_ids      = local.accepter_private_route_table_ids
  requester_private_route_table_ids     = local.requester_private_route_table_ids
  requester_private_subnets_cidr_blocks = local.requester_private_subnets_cidr_blocks
}
