locals {
  requester_private_route_table        = toset(var.requester_private_route_table_ids)
  requester_private_subnet_cidr_blocks = toset(var.requester_private_subnets_cidr_blocks)
  accepter_private_route_table         = toset(var.accepter_private_route_table_ids)
  accepter_private_subnet_cidr_blocks  = toset(var.accepter_private_subnets_cidr_blocks)

  requester_routes = [
    for pair in setproduct(local.requester_private_route_table, local.accepter_private_subnet_cidr_blocks) : {
      private_route_table       = pair[0]
      private_subnet_cidr_block = pair[1]
    }
  ]

  accepter_routes = [
    for pair in setproduct(local.accepter_private_route_table, local.requester_private_subnet_cidr_blocks) : {
      private_route_table       = pair[0]
      private_subnet_cidr_block = pair[1]
    }
  ]
}

provider "aws" {
  alias  = "accepter"
  region = var.peer_region
  assume_role {
    role_arn = var.role_arn
  }
}
resource "aws_vpc_peering_connection" "peer" {
  vpc_id        = var.requester_vpc_id
  peer_vpc_id   = var.accepter_vpc_id
  peer_region   = var.peer_region
  auto_accept   = var.auto_accept_peering
  peer_owner_id = element(split(":", var.role_arn), 4)
  tags = {
    Side = "Requester"
  }
}
resource "aws_vpc_peering_connection_accepter" "accepter" {
  provider                  = aws.accepter
  vpc_peering_connection_id = aws_vpc_peering_connection.peer.id
  auto_accept               = true
  tags = {
    Side = "accepter"
  }
}

resource "aws_vpc_peering_connection_options" "requester" {
  vpc_peering_connection_id = aws_vpc_peering_connection_accepter.accepter.id

  requester {
    allow_remote_vpc_dns_resolution  = var.requester_dns_resolution
    allow_classic_link_to_remote_vpc = var.requester_link_to_peer_classic
    allow_vpc_to_remote_classic_link = var.requester_link_to_local_classic
  }
}

resource "aws_vpc_peering_connection_options" "accepter" {
  provider                  = aws.accepter
  vpc_peering_connection_id = aws_vpc_peering_connection_accepter.accepter.id

  accepter {
    allow_remote_vpc_dns_resolution  = var.accepter_dns_resolution
    allow_classic_link_to_remote_vpc = var.accepter_link_to_peer_classic
    allow_vpc_to_remote_classic_link = var.accepter_link_to_local_classic
  }
}

resource "aws_route" "requester" {
  for_each                  = { for o in local.requester_routes : "${o.private_route_table}:${o.private_subnet_cidr_block}" => o }
  route_table_id            = each.value.private_route_table
  destination_cidr_block    = each.value.private_subnet_cidr_block
  vpc_peering_connection_id = aws_vpc_peering_connection.peer.id
}

resource "aws_route" "accepter" {
  provider                  = aws.accepter
  for_each                  = { for o in local.accepter_routes : "${o.private_route_table}:${o.private_subnet_cidr_block}" => o }
  route_table_id            = each.value.private_route_table
  destination_cidr_block    = each.value.private_subnet_cidr_block
  vpc_peering_connection_id = aws_vpc_peering_connection.peer.id
}
