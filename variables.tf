variable "role_arn" {
  description = "Assumed role for accessing accepter account"
  type        = string
}
variable "auto_accept_peering" {
  description = "Accept the peering (both VPCs need to be in the same AWS account)."
  type        = bool
  default     = false
}
variable "peer_region" {
  description = "The region of the accepter VPC of the [VPC Peering Connection]. Auto_accept_peering must be false"
  type        = string
}
variable "requester_vpc_id" {
  description = "The ID of the requester VPC."
  type        = string
}
variable "accepter_vpc_id" {
  description = "The ID of the VPC with which you are creating the VPC Peering Connection."
  type        = string
}

variable "accepter_private_subnets_cidr_blocks" {
  description = "List of IPv6 cidr_blocks of private subnets in an IPv4 enabled VPC of accepter ACC"
  type        = list(string)
}

variable "accepter_private_route_table_ids" {
  description = "List of IPv6 cidr_blocks of private subnets in an IPv4 enabled VPC of accepter ACC"
  type        = list(string)
}

variable "requester_private_route_table_ids" {
  description = "List of IPv6 cidr_blocks of private subnets in an IPv4 enabled VPC of requester ACC"
  type        = list(string)
}
variable "requester_private_subnets_cidr_blocks" {
  description = "List of IPv6 cidr_blocks of private subnets in an IPv4 enabled VPC of requester ACC"
  type        = list(string)
}

variable "requester_dns_resolution" {
  description = "Allow a local VPC to resolve public DNS hostnames to private IP addresses when queried from instances in the peer VPC."
  type        = bool
  default     = false
}

variable "requester_link_to_peer_classic" {
  description = "Allow a local linked EC2-Classic instance to communicate with instances in a peer VPC. This enables an outbound communication from the local ClassicLink connection to the remote VPC. This option is not supported for inter-region VPC peering."
  type        = bool
  default     = false
}

variable "requester_link_to_local_classic" {
  description = "Allow a local VPC to communicate with a linked EC2-Classic instance in a peer VPC. This enables an outbound communication from the local VPC to the remote ClassicLink connection. This option is not supported for inter-region VPC peering."
  type        = bool
  default     = false
}

variable "accepter_dns_resolution" {
  description = "Allow a local VPC to resolve public DNS hostnames to private IP addresses when queried from instances in the peer VPC."
  type        = bool
  default     = false
}

variable "accepter_link_to_peer_classic" {
  description = "Allow a local linked EC2-Classic instance to communicate with instances in a peer VPC. This enables an outbound communication from the local ClassicLink connection to the remote VPC. This option is not supported for inter-region VPC peering."
  type        = bool
  default     = false
}

variable "accepter_link_to_local_classic" {
  description = "Allow a local VPC to communicate with a linked EC2-Classic instance in a peer VPC. This enables an outbound communication from the local VPC to the remote ClassicLink connection. This option is not supported for inter-region VPC peering."
  type        = bool
  default     = false
}
